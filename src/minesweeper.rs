use core::fmt::Display;
use std::cmp::{max, min};
use std::{collections::HashSet, hash::Hash};

#[derive(Clone, Copy, Eq, Hash, PartialEq, Debug)]
pub struct Position {
    pub x: usize,
    pub y: usize,
}

#[derive(Debug)]
pub enum OpenResult {
    Mine,
    Nearby(u8),
}

#[derive(Debug)]
pub struct Minesweeper {
    width: usize,
    height: usize,
    open_fields: HashSet<Position>,
    mines: HashSet<Position>,
    flagged_fields: HashSet<Position>,
    lost: bool,
}

impl Display for Minesweeper {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                let pos = Position { x: x, y: y };

                if self.lost && self.mines.contains(&pos) {
                    f.write_str("💣\t")?;
                } else if self.flagged_fields.contains(&pos) {
                    f.write_str("🚩\t")?;
                } else if !self.open_fields.contains(&pos) {
                    f.write_str("🟪\t")?;
                } else if self.mines.contains(&pos) {
                    f.write_str("💣\t")?;
                } else {
                    let mine_count = self.nearby_mines(pos);
                    if mine_count == 0 {
                        f.write_str("⬜\t")?;
                    } else {
                        write!(f, "{}\t", mine_count)?;
                    }
                }
            }
            f.write_str("\n");
        }
        Ok(())
    }
}

impl Minesweeper {
    pub fn new(width: usize, height: usize, mine_count: usize) -> Minesweeper {
        Minesweeper {
            width: width,
            height: height,
            open_fields: HashSet::new(),
            mines: {
                let mut mines: HashSet<Position> = HashSet::new();

                while mines.len() < min(mine_count, width * height) {
                    mines.insert(Position {
                        x: crate::random::random_range(0, width),
                        y: crate::random::random_range(0, height),
                    });
                }

                mines
            },
            flagged_fields: HashSet::new(),
            lost: false,
        }
    }

    pub fn iter_neighbours(&self, position: Position) -> impl Iterator<Item = Position> {
        let width = self.width;
        let height = self.height;

        (max(position.x, 1) - 1..=min(position.x + 1, width - 1))
            .flat_map(move |i| {
                (max(position.y, 1) - 1..=min(position.y + 1, height - 1))
                    .map(move |j| Position { x: i, y: j })
            })
            .filter(move |p| p != &position)
    }

    pub fn nearby_mines(&self, position: Position) -> u8 {
        self.iter_neighbours(position)
            .filter(|p| self.mines.contains(p))
            .count()
            .try_into()
            .unwrap()
    }

    pub fn open(&mut self, position: Position) -> Option<OpenResult> {
        if self.open_fields.contains(&position) {
            let mine_count = self.nearby_mines(position);
            let flag_count = self
                .iter_neighbours(position)
                .filter(|nearby| self.flagged_fields.contains(nearby))
                .count() as u8;

            if mine_count == flag_count {
                for nearby in self.iter_neighbours(position) {
                    if !self.flagged_fields.contains(&nearby) && !self.open_fields.contains(&nearby)
                    {
                        self.open(nearby);
                    }
                }
            }
            return None;
        }

        if self.lost || self.flagged_fields.contains(&position) {
            return None;
        }

        self.open_fields.insert(position);

        if self.mines.contains(&position) {
            self.lost = true;
            return Some(OpenResult::Mine);
        } else {
            let mine_count = self.nearby_mines(position);

            if mine_count == 0 {
                for nearby in self.iter_neighbours(position) {
                    if !self.open_fields.contains(&nearby) {
                        self.open(nearby);
                    }
                }
            }

            return Some(OpenResult::Nearby(mine_count));
        };
    }

    pub fn toggle_flag(&mut self, position: Position) {
        if self.lost || self.open_fields.contains(&position) {
            return;
        }

        if self.flagged_fields.contains(&position) {
            self.flagged_fields.remove(&position);
        } else {
            self.flagged_fields.insert(position);
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn picked_nearby() {
        let mut game = crate::Minesweeper::new(3, 3, 9);

        game.mines.remove(&crate::Position { x: 1, y: 1 });

        let p = crate::Position { x: 1, y: 1 };

        let a = game.open(p);

        println!("game {:?}", game);
        println!("count {:?}", game.iter_neighbours(p).count());
        println!("mine len {:?}", game.mines.len());
        println!("a {:?}", a);
    }
}
