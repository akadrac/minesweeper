use minesweeper::{Minesweeper, OpenResult, Position};

#[cfg(test)]
mod tests {

    #[test]
    fn test() {
        let game = crate::Minesweeper::new(10, 10, 5);
        println!("{:?}", game);
    }

    #[test]
    fn picked_mine() {
        let mut game = crate::Minesweeper::new(1, 1, 1);
        let a = game.open(crate::Position { x: 0, y: 0 });

        println!("{:?}", game);
        println!("{:?}", a);
    }

    #[test]
    fn picked_safe() {
        let mut game = crate::Minesweeper::new(1, 1, 0);
        let a = game.open(crate::Position { x: 0, y: 0 });

        println!("{:?}", game);
        println!("{:?}", a);
    }

    #[test]
    fn picked_more() {
        let mut game = crate::Minesweeper::new(3, 3, 10);
        let a = game.open(crate::Position { x: 0, y: 0 });

        println!("{:?}", game);
        println!("{:?}", a);
    }

    #[test]
    fn picked_high() {
        let mut game = crate::Minesweeper::new(3, 3, 0);
        let a = game.open(crate::Position { x: 2, y: 2 });

        println!("{:?}", game);
        println!("{:?}", a);
    }

    #[test]
    fn picked_low() {
        let mut game = crate::Minesweeper::new(3, 3, 0);
        let a = game.open(crate::Position { x: 0, y: 0 });

        println!("{:?}", game);
        println!("{:?}", a);
    }

    #[test]
    fn display_map() {
        let mut game = crate::Minesweeper::new(9, 9, 5);

        game.open(crate::Position { x: 0, y: 0 });

        game.open(crate::Position { x: 4, y: 4 });

        game.open(crate::Position { x: 8, y: 8 });

        game.toggle_flag(crate::Position { x: 2, y: 2 });

        game.open(crate::Position { x: 2, y: 2 });

        println!("{}", game);
    }
}
